import * as React from "react";
import ImportantLink from "../components/ImportantLink";
import YTHomePage from "../components/YTHomePage";
import Footer from "../components/Footer";

export default function Home() {
  return (
    <div>
      <ImportantLink />
      <YTHomePage />
      <Footer />
    </div>
  );
}
