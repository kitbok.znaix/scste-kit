import React from "react";
import { useState } from "react";
import { graphql } from "gatsby";

// FontAwesome
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faAngleRight } from "@fortawesome/free-solid-svg-icons";
import { faAngleLeft } from "@fortawesome/free-solid-svg-icons";

//Plugin
import Lightbox from "yet-another-react-lightbox";
import Zoom from "yet-another-react-lightbox/plugins/zoom";
import Fullscreen from "yet-another-react-lightbox/plugins/fullscreen";
import Thumbnails from "yet-another-react-lightbox/plugins/thumbnails";
import Counter from "yet-another-react-lightbox/plugins/counter";
import Download from "yet-another-react-lightbox/plugins/download";
import Share from "yet-another-react-lightbox/plugins/share";

// CSS
import "yet-another-react-lightbox/styles.css";
import "yet-another-react-lightbox/plugins/counter.css";
import "yet-another-react-lightbox/plugins/thumbnails.css";

export const data = graphql`
  query {
    allNodeGallery {
      nodes {
        title
        relationships {
          field_imagegallery {
            localFile {
              childImageSharp {
                gatsbyImageData
              }
            }
          }
        }
      }
    }
  }
`;

function Gallery({ data }) {
  const image = data.allNodeGallery.nodes[0];

  const imageGallery = image.relationships.field_imagegallery.map(
    (imageData) => {
      const imageGatsby = imageData.localFile.childImageSharp;
      return imageGatsby.gatsbyImageData.images.fallback.src;
    }
  );

  const [index, setIndex] = useState(-1);
  const [currentPage, setCurrentPage] = useState(1);
  const galleryPerPage = 10;
  const lastPage = currentPage * galleryPerPage;
  const firstPage = lastPage - galleryPerPage;

  // slice imageGallery[] so it will have only 10 record
  const imageRecord = imageGallery.slice(firstPage, lastPage);
  // Total number of pages
  const nPage = Math.ceil(imageGallery.length / galleryPerPage);
  const number = [...Array(nPage + 1).keys()].slice(1);

  const openLightbox = (index) => {
    setIndex(index);
  };

  const closeLightbox = () => {
    setIndex(-1);
  };

  function prePage() {
    if (currentPage !== 1) {
      setCurrentPage(currentPage - 1);
    }
  }

  function changeCurrentPage(id) {
    setCurrentPage(id);
  }

  function nextPage() {
    if (currentPage !== nPage) {
      setCurrentPage(currentPage + 1);
    }
  }

  console.log("imageRecord=>", imageRecord);

  return (
    <>
      <div className="overflow-hidden p-14">
        <p className="mb-3 text-xl text-center">Gallery</p>
        <div className="flex justify-center items-center">
          <div className="grid grid-cols-1 gap-3 md:grid-cols-3 sm:grid-cols-2 lg:grid-cols-5">
            {imageRecord.map((imagePath, index) => (
              <div key={index}>
                <img
                  src={imagePath}
                  className="object-cover w-full h-full rounded-lg transition-all duration-300 ease-in-out hover:cursor-pointer hover:opacity-80"
                  onClick={() => openLightbox(index)}
                />
              </div>
            ))}
          </div>
        </div>
        <Lightbox
          index={index}
          slides={imageRecord.map((photo) => ({ src: photo }))}
          plugins={[Thumbnails, Zoom, Fullscreen, Counter, Download, Share]}
          open={index >= 0}
          close={closeLightbox}
        />
      </div>
      <nav className="mt-6 w-full">
        <ul className="flex justify-center items-center">
          <li>
            <FontAwesomeIcon
              onClick={prePage}
              icon={faAngleLeft}
              className={`p-2 m-2 text-3xl rounded-full border hover:cursor-pointer ${
                currentPage !== 1 ? "shadow-lg" : "text-gray-400"
              }`}
            />
          </li>
          {number.map((n, i) => (
            <li
              key={i}
              onClick={() => changeCurrentPage(n)}
              className={`rounded-full border m-2 p-2 hover:cursor-pointer ${
                currentPage === n
                  ? "text-emerald-500 border-green-200 scale-110 shadow-lg"
                  : ""
              }`}
            >
              {n}
            </li>
          ))}
          <li>
            <FontAwesomeIcon
              onClick={nextPage}
              icon={faAngleRight}
              className={`p-2 m-2 text-3xl rounded-full border hover:cursor-pointer ${
                currentPage !== nPage ? "shadow-lg" : "text-gray-400"
              }`}
            />
          </li>
        </ul>
      </nav>
    </>
  );
}

export default Gallery;
