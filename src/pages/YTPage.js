import React from "react";
import { useState } from "react";
import { graphql } from "gatsby";

// CSS
import "yet-another-react-lightbox/styles.css";
import "yet-another-react-lightbox/plugins/counter.css";
import "yet-another-react-lightbox/plugins/thumbnails.css";

export const data = graphql`
  query {
    allNodeYoutubeVideos {
      nodes {
        title
        field_yt_videos {
          processed
        }
      }
    }
  }
`;

function YTPage({ data }) {
  const YTAllVideos = data.allNodeYoutubeVideos.nodes;
  const YTArray = YTAllVideos.map((video) => {
    return {
      YTTitle: video.title,
      YTVideo: video.field_yt_videos.map((item) => item.processed),
    };
  });

  console.log("YT=>", YTArray);

  const [selectedTitle, setSelectedTitle] = useState(0); // Set the default to [0]

  return (
    <div className="">
      <p className="text-center">CATEGORY</p>
      <div className="mb-12 w-full text-center">
        <select onChange={(e) => setSelectedTitle(e.target.value)}>
          {YTArray.map((video, index) => (
            <option key={index} value={index}>
              {video.YTTitle}
            </option>
          ))}
        </select>
      </div>
      <div className="grid grid-cols-4 place-items-center">
        {/* <h2>{YTArray[selectedTitle].YTTitle}</h2> */}
        {YTArray[selectedTitle].YTVideo.map((videoContent, videoIndex) => (
          <div key={videoIndex} className="">
            <div
              dangerouslySetInnerHTML={{
                __html: videoContent.replace(
                  "<iframe",
                  '<iframe width="360" height="250""'
                ),
              }}
            />
          </div>
          // <div key={videoIndex} className="">
          //   <div dangerouslySetInnerHTML={{ __html: videoContent }} />
          // </div>
        ))}
      </div>
    </div>
  );
}

export default YTPage;
