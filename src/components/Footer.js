import { Link } from "gatsby";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFacebook } from "@fortawesome/free-brands-svg-icons";
import { faInstagram } from "@fortawesome/free-brands-svg-icons";
import { faEnvelope } from "@fortawesome/free-regular-svg-icons";
import { faPhone } from "@fortawesome/free-solid-svg-icons";
import React from "react";

function Footer() {
  return (
    <footer className="mt-6 w-full text-slate-200">
      <div className="grid grid-cols-1 gap-1 text-center lg:grid-cols-6 md:grid-cols-4 sm:grid-cols-2 lg:text-start md:text-start sm:text-start bg-slate-900">
        <div className="p-6 lg:py-6 lg:pl-20 sm:pl-28 md:pl-20">
          <h2 className="font-semibold">Pages</h2>
          <ul className="text-slate-500">
            <li className="mb-1 hover:text-blue-400">
              <Link to="/">Home</Link>
            </li>
            <li className="mb-1 hover:text-blue-400">
              <Link to="/Activities">Activities</Link>
            </li>
            <li className="mb-1 hover:text-blue-400">
              <Link to="/Programmers">Programmers</Link>
            </li>
            <li className="mb-1 hover:text-blue-400">
              <Link to="/Success">Success</Link> Stories
            </li>
            <li className="mb-1 hover:text-blue-400">
              <Link to="/Impact">Impact</Link>
            </li>
            <li className="mb-1 hover:text-blue-400">
              <Link to="/Gallery">Gallery</Link>
            </li>
          </ul>
        </div>
        <div className="p-6">
          <h2 className="font-semibold">Institution</h2>
          <ul className="text-slate-500">
            <li className="mb-1 hover:text-blue-400">
              <Link to="/">SCSTE</Link>
            </li>
            <li className="mb-1 hover:text-blue-400">
              <Link
                target="_blank"
                rel="noopener noreferrer"
                className="hover:text-blue-600"
                href="https://megplanning.gov.in/"
              >
                State Planning Department
              </Link>
            </li>
            <li className="mb-1 hover:text-blue-400">
              <Link
                target="_blank"
                rel="noopener noreferrer"
                className="hover:text-blue-600"
                href="https://mbda.gov.in/"
              >
                Meghalaya Basin Development Authority
              </Link>
            </li>
          </ul>
        </div>
        <div className="hidden col-span-2 py-6 text-center lg:block">
          <img className="max-w-[200px] mx-auto" src="/logoWhite.png" />
          <p className="mt-2 font-bold">
            State Council of Science Technology & Environment,
            <span className="ml-2 text-blue-800">MEGHALAYA</span>
          </p>
        </div>
        <div className="p-6 sm:pl-28 md:p-6">
          <h2 className="font-semibold">Contact Us</h2>
          <ul className="text-slate-500">
            <Link href="mailto:stcouncil-megh@meghalaya.gov.in">
              <li className="mb-1 hover:text-blue-400">
                <FontAwesomeIcon icon={faEnvelope} />
                &nbsp;stcouncil-megh@meghalaya.gov.in
              </li>
            </Link>
            <Link href="tel:+913642522077">
              <li className="mb-1 hover:text-blue-400">
                <FontAwesomeIcon icon={faPhone} />
                &nbsp;+91 3642522077
              </li>
            </Link>
          </ul>
        </div>
        <div className="p-6">
          <h2 className="font-semibold">Follow Us</h2>
          <ul className="text-slate-500">
            <Link
              target="_blank"
              rel="noopener noreferrer"
              href="https://www.facebook.com/people/Scste-Meghalaya/100016522481452/"
            >
              <li className="mb-1 hover:text-blue-600">
                <FontAwesomeIcon icon={faFacebook} />
                &nbsp;Facebook
              </li>
            </Link>
            <Link
              target="_blank"
              rel="noopener noreferrer"
              href="https://www.instagram.com/stcouncilmegh/"
            >
              <li className="mb-1 hover:text-orange-400">
                <FontAwesomeIcon icon={faInstagram} />
                &nbsp;Instagram
              </li>
            </Link>
          </ul>
        </div>
      </div>
      <div className="px-3 py-2 m-0 mx-auto w-full text-xs text-center text-slate-400 bg-slate-950">
        <p>&copy; 2017. All rights reserved.</p>
        <p>
          Content maintained and updated by State Council of Science Technology
          & Environment, Government of Meghalaya
        </p>
        <p>
          All Queries/Comments regarding the content on this site may be sent
          to&nbsp;
          <Link
            href="mailto:stcouncil-megh@meghalaya.gov.in"
            className="hover:text-blue-400"
          >
            <FontAwesomeIcon icon={faEnvelope} />
            stcouncil-megh@meghalaya.gov.in
          </Link>
        </p>
      </div>
    </footer>
  );
}

export default Footer;
