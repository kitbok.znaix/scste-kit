import React from "react";
import { Link } from "gatsby";

function ImportantLink() {
  return (
    <aside className="w-56 rounded-lg border shadow-md">
      <h2 className="p-4 font-semibold text-center">🔗Important Links</h2>
      <hr className="mx-auto w-1/2" />
      <ul className="p-6 pl-10 list-disc">
        <li className="mb-2">
          <Link
            target="_blank"
            rel="noopener noreferrer"
            className="hover:text-blue-600"
            href="https://india.gov.in/"
          >
            India National Portal
          </Link>
        </li>
        <li className="mb-2">
          <Link
            target="_blank"
            rel="noopener noreferrer"
            className="hover:text-blue-600"
            href="https://meghalaya.gov.in/"
          >
            Meghalaya Web Portal
          </Link>
        </li>
        <li className="mb-2">
          <Link
            target="_blank"
            rel="noopener noreferrer"
            className="hover:text-blue-600"
            href="https://megplanning.gov.in/"
          >
            State Planning Department
          </Link>
        </li>
        <li className="mb-2">
          <Link
            target="_blank"
            rel="noopener noreferrer"
            className="hover:text-blue-600"
            href="https://mbda.gov.in/"
          >
            MBDA
          </Link>
        </li>
      </ul>
    </aside>
  );
}

export default ImportantLink;
